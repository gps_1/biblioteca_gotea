import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

/*Contenedor del arbol de navegacion*/
const Stack = createStackNavigator();

//Import de screens
import Login from './src/screens/Login';
import Registro from './src/screens/Registro';
import ListadoLibros from './src/screens/ListadoLibros';
import DetalleLibro from './src/screens/DetalleLibro';
import ListadoLibrosInvitado from './src/screens/ListadoLibrosInvitado';
import PrincipalAdmin from './src/screens/PrincipalAdmin';
import PrincipalBibliotecario from './src/screens/PrincipalBibliotecario';
import ListaLibrosAdmin from './src/screens/ListaLibrosAdmin';
import ListaUsuarios from './src/screens/ListaUsuarios';
import AgregarLibro from './src/screens/AgregarLibro';
import EditarLibro from './src/screens/EditarLibro';
import AgregarUsuario from './src/screens/AgregarUsuario';
import EditarUsuario from './src/screens/EditarUsuario';
import SolicitarPrestamo from './src/screens/SolicitarPrestamo';
import BuscarLinea from './src/screens/BuscarLinea';
import PrincipalInvitado from './src/screens/PrincipalInvitado';
import SubirImagen from './src/screens/SubirImagen';
export default function App() {
  return (
    <NavigationContainer>
            <Stack.Navigator>
            <Stack.Screen
                  name='Login'
                  component={Login}
                  options = {{title : 'Biblioteca GOTEA'}}
              />
            <Stack.Screen
                  name='Registro'
                  component={Registro}
              />
            <Stack.Screen
                  name='ListadoLibrosInvitado'
                  component={ListadoLibrosInvitado}
                  options = {{title : 'Listado de Libros'}}
              />
            <Stack.Screen
                  name='ListadoLibros'
                  component={ListadoLibros}
                  options = {{title : 'Listado de Libros'}}
              />
            <Stack.Screen
                  name='DetalleLibro'
                  component={DetalleLibro}
                  options = {{title : 'Detalle de Libro'}}
              />
            <Stack.Screen
                  name='PrincipalBibliotecario'
                  component={PrincipalBibliotecario}
                  options = {{title : 'Bienvenido Bibliotecario'}}
              />
            <Stack.Screen
                  name='PrincipalAdmin'
                  component={PrincipalAdmin}
                  options = {{title : 'Bienvenido Administrador'}}
              />
            <Stack.Screen
                  name='ListaLibrosAdmin'
                  component={ListaLibrosAdmin}
                  options = {{title : 'Listado de Libros'}}
              />
            <Stack.Screen
                  name='ListaUsuarios'
                  component={ListaUsuarios}
                  options = {{title : 'Lista de Usuarios'}}
              />
            <Stack.Screen
                  name='AgregarLibro'
                  component={AgregarLibro}
                  options = {{title : 'Agregar libro 1/2'}}
              />
            <Stack.Screen
                  name='EditarLibro'
                  component={EditarLibro}
                  options = {{title : 'Editar Libro'}}
              />
            <Stack.Screen
                  name='AgregarUsuario'
                  component={AgregarUsuario}
                  options = {{title : 'Crear Nuevo Usuario'}}
              />
            <Stack.Screen
                  name='EditarUsuario'
                  component={EditarUsuario}
                  options = {{title : 'Editar Usuario'}}
              />
            <Stack.Screen
                  name='SolicitarPrestamo'
                  component={SolicitarPrestamo}
                  options = {{title : 'Solicitar Prestamo de Libro'}}
              />
              <Stack.Screen
                  name='BuscarLinea'
                  component={BuscarLinea}
                  options = {{title : 'Búsqueda en línea'}}
              />
              <Stack.Screen
                  name='PrincipalInvitado'
                  component={PrincipalInvitado}
                  options = {{title : 'Bienvenido'}}
              />
              <Stack.Screen
                  name='SubirImagen'
                  component={SubirImagen}
                  options = {{title : 'Agregar libro 2/2'}}
              />
            </Stack.Navigator>
    </NavigationContainer>
  );
}

