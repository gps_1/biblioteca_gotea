import React, { useState } from 'react';
import { Alert, ScrollView, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { estilo, baseUrl } from '../libs/MiTema';
import Loader from '../libs/Loader';
import Axios from 'axios';
import md5 from 'react-native-md5';

export default function Registro(props) {

    /*Estado para mostrar y quitar el loader*/
    const [cargando, setCargando] = useState(false);

    /*Estado para guardar el correo*/
    const [correo, setCorreo] = useState('');

    /*Estado para guardar la contraseña*/
    const [password, setPassword] = useState('');

    /*Estado para guardar la contraseña 2 de usuario y validar que su contraseña
    sea la que ingreso en el primer campo*/
    const [password2, setPassword2] = useState('');

    /*Estado para guardar el nombre de usuario*/
    const [nombre, setNombre] = useState('');

    /*Estado para guardar el ap1 de usuario*/
    const [ap1, setAp1] = useState('');

    /*Estado para guardar el ap2 de usuario*/
    const [ap2, setAp2] = useState('');

    return cargando ? <Loader
        mensaje='Cargando, por favor espere...' /> : (
        <ScrollView style={estilo.back}>
            <View style={estilo.container}>
                <Text style={{ fontSize: 30, flex: 2, margin: 50, color: '#fff' }}>REGISTRO</Text>
            </View>

            {/* Inputs para ingresar datos del usuario */}
            <TextInput
                placeholder='Nombre(s)'
                keyboardType='default'
                style={estilo.input}
                onChange={val => setNombre(val.nativeEvent.text)}
                maxLength={50}
            />

            <TextInput
                placeholder='Apellido 1'
                keyboardType='default'
                style={estilo.input}
                onChange={val => setAp1(val.nativeEvent.text)}
                maxLength={50}
            />

            <TextInput
                placeholder='Apellido 2'
                keyboardType='default'
                style={estilo.input}
                onChange={val => setAp2(val.nativeEvent.text)}
                maxLength={50}
            />

            <TextInput
                placeholder='Correo Electrónico'
                keyboardType='email-address'
                style={estilo.input}
                onChange={val => setCorreo(val.nativeEvent.text)}
                maxLength={100}
            />

            <TextInput
                placeholder='Contraseña'
                keyboardType='default'
                style={estilo.input}
                onChange={val => setPassword(val.nativeEvent.text)}
                secureTextEntry={true}
                maxLength={12}
            />

            <TextInput
                placeholder='Validar Contraseña'
                keyboardType='default'
                style={estilo.input}
                onChange={val => setPassword2(val.nativeEvent.text)}
                secureTextEntry={true}
                maxLength={12}
            />

            <TouchableOpacity
                style={estilo.boton}
                onPress={() => {
                    /* Validación de campos completos */
                    if (correo === '' || password === '' || password2 === '' || nombre === '' || ap1 === '' || ap2 === '') {
                        Alert.alert(
                            'Campos vacios',
                            `Completa todos los campos`,
                            [
                                {
                                    text: 'Aceptar',
                                }
                            ],
                            { cancelable: false }

                        );
                    }
                    else if (password !== password2) {
                        Alert.alert(
                            'Contraseña Diferente',
                            `Escriba la misma contraseña en ambos campos`,
                            [
                                {
                                    text: 'Aceptar',
                                }
                            ],
                            { cancelable: false }

                        );
                    }
                    else if (password.length < 8 || password2.length < 8) {
                        Alert.alert(
                            'Contraseña invalida',
                            `Debe contener al menos 8 caracteres`,
                            [
                                {
                                    text: 'Aceptar',
                                }
                            ],
                            { cancelable: false }

                        );
                    }
                    else {
                        setCargando(true);

                        // Aquí van los datos como están en la BD | Los estados que fueron obtenidos de los input
                        const params = JSON.stringify({
                            "Nombre": nombre,
                            "Apellido1": ap1,
                            "Apellido2": ap2,
                            "Email": correo,
                            "Password": md5.hex_md5(password),
                            "Tipo_usuario": 1
                        });

                        // Función donde se realiza la petición al servidor para enviar los datos
                        Axios({
                            method: 'post',
                            url: `${baseUrl}/Usuarios/AddUsuario`,
                            data: params,
                            headers: { 'Content-Type': 'application/json' }
                        }).then((response) => {
                            console.log(response);
                            setCargando(false);
                            Alert.alert(
                                'Registro Exitoso',
                                'Ahora puedes ingresar a la aplicación',
                                [
                                    {
                                        text: 'Continuar',
                                        onPress: () => props.navigation.replace('Login')
                                    }
                                ],
                                {
                                    cancelable: false
                                }
                            );
                        }).catch((error) => {
                            setCargando(false);
                            console.log(error);
                            Alert.alert(
                                'Error',
                                'No fue posible registrarse, intente nuevamente'
                            );
                        });
                    }
                }}
            >
                <View>
                    <Text style={{ textAlign: 'center', color: '#fff' }}>
                        Registrarme
                    </Text>
                </View>
            </TouchableOpacity>

        </ScrollView>
    );
}