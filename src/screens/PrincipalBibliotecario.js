import React, { useState, useEffect } from 'react';
import { Alert, Button, FlatList, Image, View, Text } from 'react-native';
import { baseUrl } from '../libs/MiTema';
import Loader from '../libs/Loader';
import Axios from 'axios';

export default function PrincipalBibliotecario(props) {

  /*Estado para mostrar y quitar el loader*/
  const [cargando, setCargando] = useState(false);

  // Estado para guardar el arreglo de objetos libro
  const [listaPrestamos, setListaPrestamos] = useState([]);

  // Función donde se realiza la petición al servidor para traer los libros
  useEffect(() => {
    setCargando(true);
    async function getPrestamos() {
      await Axios({
        method: "get",
        url: `${baseUrl}/Prestamos/GetPrestamos`
      }).then((response) => {
        setCargando(false);
        setListaPrestamos(response.data);
      }).catch(() => {
        setCargando(false);
        Alert.alert(
          'Error',
          'No fue posible ver las solicitudes de prestamos, intente nuevamente'
        );
      });
    }
    getPrestamos();
  }, []);

  return cargando ? <Loader
    mensaje='Cargando, por favor espere...' /> : (
    <View style={{ flex: 14, backgroundColor:"#FFF" }}>
      {/* Este Flatlist es plano, solo se visualizan los libros */}
      <FlatList
        data={listaPrestamos}
        renderItem={(item, index) => {
          return (
            <View
              style={{
                marginVertical: 5,
                marginHorizontal: 5,
                marginBottom: 10
              }}
            >
              <Text style={{ fontWeight: 'bold', alignSelf: 'center', fontSize: 25 }}>{item.item.titulo_libro}</Text>
              <Image source={{ uri: item.item.imagen_libro }}
                style={{ width: 200, height: 300, alignSelf: 'center' }} />
              <Text style={{ fontWeight: 'bold', alignSelf: 'center', fontSize: 19 }}> Solicitante: {item.item.nombre_usuario} {item.item.apellido1_usuario} {item.item.apellido2_usuario}
              </Text>

              <Button
                color='tomato'
                title='Aceptar Prestamo'
                onPress={() => {
                  Alert.alert(
                    'Aceptar Prestamo',
                    '¿Realmente deseas aceptar este prestamo?',
                    [
                      {
                        text: 'No'
                      },
                      {
                        text: 'Sí',
                        onPress: () => {
                          setCargando(true);
                          Axios({
                            method: 'get',
                            url: `${baseUrl}/Prestamos/AprovePrestamo?Id=${item.item.idPrestamo}`
                          }).then(() => {
                            setCargando(false);
                            Alert.alert(
                              'Prestamo Aceptado',
                              'El libro puede ser recogido por su solicitante',
                              [
                                {
                                  text: 'Continuar',
                                  onPress: () => props.navigation.replace('Login')
                                }
                              ],
                              {
                                cancelable: false
                              }
                            );
                          }).catch(() => {
                            setCargando(false);
                            Alert.alert(
                              'Error',
                              'No fue posible actualizar el estatus, intente nuevamente'
                            );
                          })
                        }
                      }
                    ]
                  );
                }}
              >
              </Button>
            </View>
          );
        }}
      />
      <Button
        color='tomato'
        title='CERRAR SESIÓN'
        onPress={() => {
          Alert.alert('Alerta', `¿Estas seguro de salir?`,
            [
              {
                text: 'Cancelar',
              }
              ,
              {
                text: 'Aceptar',
                onPress: () => {
                  props.navigation.replace('Login');
                }
              }
            ],
            {
              cancelable: false
            }
          );
        }}
      />
    </View>
  );
}