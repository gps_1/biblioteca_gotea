import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, PixelRatio, TouchableOpacity, Image, Alert, ScrollView } from 'react-native';
import { baseUrl,baseUrl2 } from '../libs/MiTema';
import { FontAwesome5, SimpleLineIcons } from '@expo/vector-icons';
import * as Permissions from 'expo-permissions'
import Constants from 'expo-constants'
import * as ImagePicker from 'expo-image-picker';
import Loader from '../libs/Loader';
import Axios from 'axios';
let i = '';
export default function SubirImagen(props) {
    /*Estado para mostrar y quitar el loader*/
    const [cargando, setCargando] = useState(false);

    /*Estado para guardar el nombre*/
    const [nombre, setNombre] = useState(props.route.params.nombre);

    /*Estado para guardar el nombre de autor*/
    const [autor, setAutor] = useState(props.route.params.autor);

    /*Estado para guardar el status*/
    const [status, setStatus] = useState(props.route.params.status);

    const [imagen, setImagen] = useState(`${baseUrl2}static/images/no_image.jpg`);

    function subir1() {
        let filename = imagen.split('/').pop();
        let match = /\.(\w+)$/.exec(filename);
        let type = match ? `image/${match[1]}` : `image`
        let formData = new FormData();
        formData.append('nom', "pruebas");
        formData.append('imagen', { type: type, uri: imagen, name: filename });
        console.log(formData);
        Axios({
            method: 'post',
            url: `${baseUrl2}Fotos2/subir_imagen`,
            headers: { 'Content-Type': 'multipart/form-data' },
            data: formData,
        }).then(json => {
            console.log(json.data);
            if (json.data.response_code === 200) {

                i = `${baseUrl2}static/images/prestadores/${json.data.response_data}`;

                // Aquí van los datos como están en la BD | Los estados que fueron obtenidos de los input
                const params = JSON.stringify({
                    "Titulo": nombre,
                    "Autor": autor,
                    "Imagen": i,
                    "Estado": status
                });

                // Función donde se realiza la petición al servidor para enviar los datos
                Axios({
                    method: 'post',
                    url: `${baseUrl}/Libros/AddLibro`,
                    data: params,
                    headers: { 'Content-Type': 'application/json' }
                }).then(() => {
                    setCargando(false);
                    Alert.alert(
                        'Registro Exitoso',
                        'Los datos del libro fueron creados con exito',
                        [
                            {
                                text: 'Continuar',
                                onPress: () => props.navigation.navigate('PrincipalAdmin')
                            }
                        ],
                        {
                            cancelable: false
                        }
                    );
                }).catch(() => {
                    setCargando(false);
                    Alert.alert(
                        'Error',
                        'No fue posible hacer el registro, intente nuevamente'
                    );
                    setCargando(false);
                });
            }
            else {
                Alert.alert(
                    'ERROR',
                    'Ocurrió un algo innesperado, por favor intentalo mas tarde');
                    setCargando(false);
            }

        }).catch(e => {
            Alert.alert(
                'ERROR',
                'Ocurrió un error, por favor intentalo mas tarde');
            console.log(e);
            setCargando(false);
            console.log(e);
        });
    }

    return cargando ? <Loader mensaje='Por favor espere' /> : (
        <ScrollView style={{flexGrow:1, flex:1, backgroundColor:"#FFF" }}>
            <View style={styles.container}>

                <Text style={styles.text2}>Imagen del libro</Text>
                <View style={styles.ImageContainer}>
                    {imagen === null ? <Text style={styles.text}>Seleccciona una imagen</Text> :
                        <Image style={styles.ImageContainer} source={{ uri: imagen === null ? `${baseUrl2}static/images/no_image.jpg` : imagen }} />}
                </View>
                <View style={{ flexDirection: 'row', }}>
                    <TouchableOpacity style={{ backgroundColor: '#358CF4', borderRadius: 10, margin: 5, alignItems: 'center' }}
                        onPress={async () => {
                            if (Constants.platform.ios || Constants.platform.android) {
                                const permisoGaleria = await Permissions.askAsync(Permissions.CAMERA);
                                if (permisoGaleria.status === 'granted') {
                                    try {
                                        let imagenGaleria = await ImagePicker.launchImageLibraryAsync({
                                            mediaTypes: ImagePicker.MediaTypeOptions.Images,
                                            allowsEditing: true,
                                            aspect: [4, 4],
                                            quality: 1
                                        });
                                        if (imagenGaleria.cancelled === false) {
                                            setImagen(imagenGaleria.uri);
                                        }
                                        else {
                                            setImagen(`${baseUrl2}static/images/no_image.jpg`);
                                        }
                                    }
                                    catch (e) {
                                        console.log(e);
                                    }
                                }
                                else {
                                    Alert.alert(
                                        'ERROR',
                                        'No contamos con el permiso para acceder a la galería'
                                    )
                                    setCargando(false);
                                }
                            }
                        }}>
                        <Text>&nbsp;&nbsp;&nbsp;
                            <SimpleLineIcons name='folder-alt' size={30} color='#FFF' />&nbsp;&nbsp;&nbsp;
                        </Text>
                        {/*<Text style={{color:'#FFF'}}>&nbsp;Seleccionar&nbsp;</Text>*/}
                    </TouchableOpacity>

                    <TouchableOpacity style={{ backgroundColor: '#358CF4', borderRadius: 10, margin: 5, alignItems: 'center' }}
                        onPress={async () => {
                            const permisoCamara = await Permissions.askAsync(Permissions.CAMERA);
                            if (permisoCamara.status === 'granted') {
                                try {
                                    let imagenCamara = await ImagePicker.launchCameraAsync({
                                        allowsEditing: true,
                                        aspect: [4, 4],
                                        quality: 1
                                    });
                                    if (imagenCamara.cancelled === false) {
                                        setImagen(imagenCamara.uri)
                                    }
                                    else {
                                        setImagen(`${baseUrl2}static/images/no_image.jpg`);

                                    }
                                }
                                catch (e) {
                                    console.log(e);
                                }
                            }
                        }}>
                        <Text>&nbsp;&nbsp;&nbsp;
                            <FontAwesome5 name='camera-retro' size={30} color='#FFF' />&nbsp;&nbsp;&nbsp;
                        </Text>
                        {/*<Text style={{color:'#FFF'}}>&nbsp;&nbsp;&nbsp;Capturar&nbsp;&nbsp;&nbsp;</Text>*/}
                    </TouchableOpacity>
                </View>
                <TouchableOpacity style={{ backgroundColor: '#27D90F', borderRadius: 10, margin: 5, alignItems: 'center' }}
                    onPress={async () => {
                        setCargando(true);

                        if (imagen === `${baseUrl2}static/images/no_image.jpg`) {
                            Alert.alert(
                                'No ha seleccionado una imagen',
                                `Por favor seleccione una imagen`
                            );
                            setImagen(`${baseUrl2}static/images/no_image.jpg`);
                            setCargando(false);
                        } else {
                            subir1();
                            setCargando(false);
                        }

                    }}>
                    <Text>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <FontAwesome5 name='check-circle' size={32} color='#FFF' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </Text>
                    <Text style={{ marginHorizontal: 5, marginVertical: 5 }}>Crear libro</Text>
                </TouchableOpacity>

            </View>
        </ScrollView>
    );

}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#FFF',
        paddingTop: 20,
        flexGrow:1
    },
    text: {
        color: '#000',
        fontSize: 16,
        fontWeight: 'bold'
    },
    text2: {
        color: '#000',
        fontSize: 20,
        fontWeight: 'bold',
        fontStyle: 'italic'
    },
    ImageContainer: {
        borderRadius: 10,
        width: 250,
        height: 250,
        borderColor: '#9B9B9B',
        borderWidth: 1 / PixelRatio.get(),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFF',

    },

    TextInputStyle: {

        textAlign: 'center',
        height: 40,
        width: '80%',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#028b53',
        marginTop: 20
    },

    button: {

        width: '80%',
        backgroundColor: '#00BCD4',
        borderRadius: 7,
        marginTop: 20
    },

    TextStyle: {
        color: '#fff',
        textAlign: 'center',
        padding: 10
    }

});