import React, { useState, useEffect } from 'react';
import { Alert, Button, FlatList, Image, Text, TouchableOpacity, View } from 'react-native';
import { baseUrl } from '../libs/MiTema';
import Loader from '../libs/Loader';
import Axios from 'axios';

export default function ListadoLibros(props) {

  /*Estado para mostrar y quitar el loader*/
  const [cargando, setCargando] = useState(false);

  // Estado para guardar el arreglo de objetos libro
  const [listaLibros, setListaLibros] = useState([]);

  // Función donde se realiza la petición al servidor para traer los libros
  useEffect(() => {
    setCargando(true);
    async function getLibros() {
      await Axios({
        method: "get",
        url: `${baseUrl}/Libros/GetLibros`
      }).then((response) => {
        setCargando(false);
        setListaLibros(response.data);
      }).catch(() => {
        setCargando(false);
        Alert.alert(
          'Error',
          'No fue posible ver el listado de libros, intente nuevamente'
        );
      });
    }
    getLibros();
  }, []);

  return cargando ? <Loader
    mensaje='Cargando, por favor espere...' /> : (
    <View style={{ flex: 14, backgroundColor: "#FFF" }} >
      <FlatList
        data={listaLibros}
        keyExtractor={(item, index) => index.toString()}
        renderItem={(item, index) => {
          return (
            <TouchableOpacity key={`Libro-${index}`}
              onPress={
                () => props.navigation.navigate('DetalleLibro', {
                  id_libro: item.item.idLibro,
                  nom_libro: item.item.titulo,
                  img_libro: item.item.imagen,
                  autor_libro: item.item.autor,
                  status_disp_libro: item.item.estado,
                  id: props.route.params.id,
                  nombre: props.route.params.name,
                  ap1: props.route.params.ap1,
                  ap2: props.route.params.ap2
                })
              }
              style={{
                marginVertical: 5,
                marginHorizontal: 5,
                marginBottom: 10,
                borderWidth: 1,
                borderRadius: 5,
                borderColor: '#ddd',
                borderBottomWidth: 0,
                shadowColor: '#000000',
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.8,
                shadowRadius: 5,
                elevation: 4,

              }}
            >
              <Text key={`T-${index}`} style={{ fontWeight: 'bold', alignSelf: 'center', fontSize: 20 }}>{item.item.titulo}</Text>
              <Image key={`I-${index}`} source={{ uri: item.item.imagen }}
                style={{ width: 200, height: 300, alignSelf: 'center' }} />
            </TouchableOpacity>
          );
        }}
      />
      <Button
        color='tomato'
        title='CERRAR SESIÓN'
        onPress={() => {
          Alert.alert('Alerta', `¿Estas seguro de salir?`,
            [
              {
                text: 'Cancelar',
              }
              ,
              {
                text: 'Aceptar',
                onPress: () => {
                  props.navigation.replace('Login');
                }
              }
            ],
            {
              cancelable: false
            }
          );
        }}
      />
    </View>
  );
}