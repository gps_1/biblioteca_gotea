import React, { useState } from 'react';
import { Alert, ScrollView, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { estilo, baseUrl } from '../libs/MiTema';
import Loader from '../libs/Loader';
import Axios from 'axios';
//import md5 from 'react-native-md5';

export default function EditarUsuario(props) {

    /*Estado para mostrar y quitar el loader*/
    const [cargando, setCargando] = useState(false);

    /*Estado para id */
    const [id, setId] = useState(props.route.params.id_usuario);

    /*Estado para guardar el correo*/
    const [correo, setCorreo] = useState(props.route.params.email_usuario);

    /*Estado para guardar el nombre de usuario*/
    const [nombre, setNombre] = useState(props.route.params.nom_usuario);

    /*Estado para guardar el ap1 de usuario*/
    const [ap1, setAp1] = useState(props.route.params.ap1_usuario);

    /*Estado para guardar el ap2 de usuario*/
    const [ap2, setAp2] = useState(props.route.params.ap2_usuario);

    /*Estado para guardar el password*/
    const [password, setPassword] = useState(props.route.params.pass_usuario)

    /*Estado para guardar el tipo de usuario*/
    const [tipoUsuario, setTipoUsuario] = useState(props.route.params.tipo_usuario)


    return cargando ? <Loader
        mensaje='Cargando, por favor espere...' /> : (
        <ScrollView style={estilo.back}>
            <View style={estilo.container}>
                <Text style={{ fontSize: 30, flex: 2, margin: 50, color: '#fff' }}>EDITAR USUARIO</Text>
            </View>

            {/* Inputs para ingresar datos del usuario */}
            <Text style={{ alignSelf: 'center', fontSize: 20, fontWeight: 'bold' }}>Correo Electrónico:</Text>
            <TextInput
                value={correo}
                keyboardType='email-address'
                style={estilo.input}
                onChange={val => setCorreo(val.nativeEvent.text)}
                maxLength={100}
            />

            <Text style={{ alignSelf: 'center', fontSize: 20, fontWeight: 'bold' }}>Nombre(s):</Text>
            <TextInput
                value={nombre}
                keyboardType='default'
                style={estilo.input}
                onChange={val => setNombre(val.nativeEvent.text)}
                maxLength={30}
            />

            <Text style={{ alignSelf: 'center', fontSize: 20, fontWeight: 'bold' }}>Apellido 1:</Text>
            <TextInput
                value={ap1}
                keyboardType='default'
                style={estilo.input}
                onChange={val => setAp1(val.nativeEvent.text)}
                maxLength={30}
            />

            <Text style={{ alignSelf: 'center', fontSize: 20, fontWeight: 'bold' }}>Apellido 2:</Text>
            <TextInput
                value={ap2}
                keyboardType='default'
                style={estilo.input}
                onChange={val => setAp2(val.nativeEvent.text)}
                maxLength={30}
            />

            {/*<Text style={{alignSelf: 'center', fontSize: 20, fontWeight: 'bold'}}>Actualizar Contraseña:</Text>
                <TextInput
                    placeholder='Contraseña'
                    keyboardType='default'
                    style={estilo.input}
                    onChange={val => setPassword(val.nativeEvent.text)}
                    secureTextEntry={true}
                    maxLength={12}
                />*/}

            <TouchableOpacity
                style={estilo.boton}
                onPress={() => {
                    /* Validación de campos completos */
                    if (correo === '' || nombre === '' || ap1 === '' || ap2 === '') {
                        Alert.alert(
                            'Campos vacios',
                            `Completa todos los campos`,
                            [
                                {
                                    text: 'Aceptar',
                                }
                            ],
                            { cancelable: false }

                        );
                    }
                    else {
                        setCargando(true);
                        // Aquí van los datos como están en la BD | Los estados que fueron obtenidos de los input
                        const params = JSON.stringify({
                            "IdUsuario": id,
                            "Nombre": nombre,
                            "Apellido1": ap1,
                            "Apellido2": ap2,
                            "Email": correo,
                            "Password": password,
                            "Tipo_usuario": tipoUsuario
                        });

                        // Función donde se realiza la petición al servidor para enviar los datos
                        Axios({
                            method: 'post',
                            url: `${baseUrl}/Usuarios/UpdateUsuario`,
                            data: params,
                            headers: { 'Content-Type': 'application/json' }
                        }).then(() => {
                            setCargando(false);
                            Alert.alert(
                                'Actualización Exitosa',
                                'Los datos del usuario fueron actualizados con exito',
                                [
                                    {
                                        text: 'Continuar',
                                        onPress: () => props.navigation.navigate('PrincipalAdmin')
                                    }
                                ],
                                {
                                    cancelable: false
                                }
                            );
                        }).catch(() => {
                            setCargando(false);
                            Alert.alert(
                                'Error',
                                'No fue posible hacer el registro, intente nuevamente'
                            );
                        });
                    }
                }}>
                <View>
                    <Text style={{ textAlign: 'center', color: '#fff' }}>
                        Actualizar usuario
                    </Text>
                </View>
            </TouchableOpacity>

        </ScrollView>
    );
}