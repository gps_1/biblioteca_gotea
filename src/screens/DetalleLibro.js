import React, { useState } from 'react';
import { Alert, Image, Text, TouchableOpacity, View } from 'react-native';
import { estilo } from '../libs/MiTema';
import Loader from '../libs/Loader';

export default function DetalleLibro(props) {

    /*Estado para mostrar y quitar el loader*/
    //const [cargando, setCargando] = useState(false);

    return (
        <View style={estilo.back}>
            <Text style={{ fontSize: 22, alignSelf: 'center', fontWeight: 'bold', margin: 10 }}>
                {props.route.params.nom_libro}
            </Text>
            <Image source={{ uri: props.route.params.img_libro }} style={{ width: 200, height: 300, alignSelf: 'center' }} />
            <Text style={{ fontSize: 20, alignSelf: 'center', margin: 10 }}>
                Autor: {props.route.params.autor_libro}
            </Text>
            <Text style={{ fontSize: 20, alignSelf: 'center' }}>
                Disponible: {props.route.params.status_disp_libro ? 'Sí' : 'No'}
            </Text>
            {/* Touchable que se mostrara cuando el status sea true, si es false, se oculta */}
            <View>
                {props.route.params.status_disp_libro
                    ?
                    <TouchableOpacity
                        style={estilo.boton}
                        onPress={() =>
                            props.navigation.navigate('SolicitarPrestamo', {
                                id_libro: props.route.params.id_libro,
                                nom_libro: props.route.params.nom_libro,
                                img_libro: props.route.params.img_libro,
                                autor_libro: props.route.params.autor_libro,
                                id_usuario: props.route.params.id,
                                nom_usuario: props.route.params.nombre,
                                ap1_usuario: props.route.params.ap1,
                                ap2_usuario: props.route.params.ap2
                            })
                        }
                    >
                        <View>
                            <Text style={{ textAlign: 'center', color: '#fff' }}>
                                Solicitar prestamo
                            </Text>
                        </View>
                    </TouchableOpacity>
                    :
                    <TouchableOpacity />
                }
            </View>
            {/* Touchable que se mostrara cuando el status sea false, si es true, se oculta */}
            <View>
                {props.route.params.status_disp_libro
                    ?
                    <View />
                    :
                    <View>
                        <TouchableOpacity
                            style={estilo.boton}
                            onPress={() => {
                                Alert.alert(
                                    'Libro apartado',
                                    `Seras notificado cuando el libro este disponible`,
                                    [
                                        {
                                            text: 'Aceptar',
                                        }
                                    ],
                                    { cancelable: false }

                                );
                            }
                            }
                        >
                            <View>
                                <Text style={{ textAlign: 'center', color: '#fff' }}>
                                    Apartar Libro
                                </Text>
                            </View>
                        </TouchableOpacity>

                    </View>
                }

            </View>
        </View>
    );
}