import React, { useState, useEffect } from 'react';
import { Alert, Button, FlatList, Image, Text, TouchableOpacity, View } from 'react-native';
import Loader from '../libs/Loader';
import Axios from 'axios';
import { estilo, baseUrl } from '../libs/MiTema';

export default function ListaLibrosAdmin(props) {

  /*Estado para mostrar y quitar el loader*/
  const [cargando, setCargando] = useState(false);

  // Estado para guardar el arreglo de objetos libro
  const [listaLibros, setListaLibros] = useState([]);

  // Función donde se realiza la petición al servidor para traer los libros
  useEffect(() => {
    setCargando(true);
    async function getLibros() {
      await Axios({
        method: "get",
        url: `${baseUrl}/Libros/GetLibros`
      }).then((response) => {
        setCargando(false);
        setListaLibros(response.data);
      }).catch(() => {
        setCargando(false);
        Alert.alert(
          'Error',
          'No fue posible ver el listado de libros, intente nuevamente'
        );
      });
    }
    getLibros();
  }, []);


  return cargando ? <Loader
    mensaje='Cargando, por favor espere...' /> : (
    <View style={estilo.back}>
      <View style={{ marginHorizontal: 100 }}>
        <Button color='#ffa833' title='Agregar Nuevo Libro' onPress={() => props.navigation.navigate('AgregarLibro')}></Button>
      </View>
      <FlatList
        data={listaLibros}
        keyExtractor={(item, index) => index.toString()}
        renderItem={(item, index) => {
          return (
            <TouchableOpacity key={`Libro-${index}`}
              onPress={
                () => props.navigation.navigate('EditarLibro', {
                  id_libro: item.item.idLibro, nom_libro: item.item.titulo, img_libro: item.item.imagen,
                  autor_libro: item.item.autor, status_disp_libro: item.item.estado
                })
              }
              style={{
                marginVertical: 5,
                marginHorizontal: 5,
                marginBottom: 10,                                    
                borderWidth: 1,
                borderRadius: 5,
                borderColor: '#ddd',
                borderBottomWidth: 0,
                shadowColor: '#000000',
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.8,
                shadowRadius: 5,
                elevation: 4,
              }}
            >
              <Text key={`T-${index}`} style={{ fontWeight: 'bold', alignSelf: 'center', fontSize: 20 }}>{item.item.titulo}</Text>
              <Image key={`I-${index}`} source={{ uri: item.item.imagen }} style={{ width: 200, height: 300, alignSelf: 'center', marginVertical:10 }} />
              <View key={`V-${index}`} style={{ marginVertical: 20, paddingHorizontal: 120 }}>
                <Button
                  key={`B-${index}`}
                  color='tomato'
                  title='Eliminar Libro'
                  onPress={() => {
                    Alert.alert(
                      'Eliminar Libro',
                      '¿Realmente deseas eliminar este libro?',
                      [
                        {
                          text: 'No'
                        },
                        {
                          text: 'Sí',
                          onPress: () => {
                            setCargando(true);
                            Axios({
                              method: 'delete',
                              url: `${baseUrl}/Libros/DeleteLibro?Id=${item.item.idLibro}`
                            }).then(() => {
                              setCargando(false);
                              Alert.alert(
                                'Libro Eliminado',
                                'Los datos del libro fueron eliminados con exito',
                                [
                                  {
                                    text: 'Continuar',
                                    onPress: () => props.navigation.navigate('PrincipalAdmin')
                                  }
                                ],
                                {
                                  cancelable: false
                                }
                              );
                            }).catch(() => {
                              setCargando(false);
                              Alert.alert(
                                'Error',
                                'No fue posible hacer la eliminación del libro, intente nuevamente'
                              );
                            })
                          }
                        }
                      ]
                    );
                  }}
                >
                </Button>
              </View>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
}