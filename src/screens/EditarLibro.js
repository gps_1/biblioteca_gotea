import React, { useState } from 'react';
import { Alert, Image, ScrollView, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { estilo, baseUrl } from '../libs/MiTema';
import Loader from '../libs/Loader';
import Axios from 'axios';

export default function EditarLibro(props) {

    /*Estado para mostrar y quitar el loader*/
    const [cargando, setCargando] = useState(false);

    /*Estado para guardar el id */
    const [id, setId] = useState(props.route.params.id_libro);

    /*Estado para guardar el nombre*/
    const [nombre, setNombre] = useState(props.route.params.nom_libro);

    /*Estado para guardar la imagen*/
    const [imagen, setImagen] = useState(props.route.params.img_libro);

    /*Estado para guardar el nombre de autor*/
    const [autor, setAutor] = useState(props.route.params.autor_libro);

    /*Estado para guardar el status*/
    const [status, setStatus] = useState(props.route.params.status_disp_libro);

    return cargando ? <Loader
        mensaje='Cargando, por favor espere...' /> : (
        <ScrollView style={estilo.back}>
            <View style={estilo.container}>
                <Text style={{ fontSize: 30, flex: 2, margin: 50, color: '#fff' }}>EDITAR LIBRO</Text>
            </View>

            {/*Inputs para editar libros*/}
            <Text style={{ alignSelf: 'center', fontSize: 20, fontWeight: 'bold' }}>Nombre de libro:</Text>
            <TextInput
                keyboardType='default'
                style={estilo.input}
                value={nombre}
                onChange={val => setNombre(val.nativeEvent.text)}
                maxLength={100}
            />

            {/*<Text style={{ alignSelf: 'center', fontSize: 20, fontWeight: 'bold' }}>Imagen:</Text>
            <TextInput
                keyboardType='default'
                style={estilo.input}
                value={imagen}
                onChange={val => setImagen(val.nativeEvent.text)}
        />*/}

            <Text style={{ alignSelf: 'center', fontSize: 20, fontWeight: 'bold' }}>Autor:</Text>
            <TextInput
                keyboardType='default'
                style={estilo.input}
                value={autor}
                onChange={val => setAutor(val.nativeEvent.text)}
                maxLength={100}
            />

            <TouchableOpacity
                style={estilo.boton}
                onPress={() => {
                    /* Validación de campos completos */
                    if (nombre === '' || imagen === '' || autor === '') {
                        Alert.alert(
                            'Campos vacios',
                            `Completa todos los campos`,
                            [
                                {
                                    text: 'Aceptar',
                                }
                            ],
                            { cancelable: false }

                        );
                    }
                    else {
                        setCargando(true);
                        // Aquí van los datos como están en la BD | Los estados que fueron obtenidos de los input
                        const params = JSON.stringify({
                            "IdLibro": id,
                            "Titulo": nombre,
                            "Autor": autor,
                            "Imagen": imagen,
                            "Estado": status
                        });

                        // Función donde se realiza la petición al servidor para enviar los datos
                        Axios({
                            method: 'post',
                            url: `${baseUrl}/Libros/UpdateLibro`,
                            data: params,
                            headers: { 'Content-Type': 'application/json' }
                        }).then(() => {
                            setCargando(false);
                            Alert.alert(
                                'Actualización Exitosa',
                                'Los datos del libro fueron actualizados con exito',
                                [
                                    {
                                        text: 'Continuar',
                                        onPress: () => props.navigation.navigate('PrincipalAdmin')
                                    }
                                ],
                                {
                                    cancelable: false
                                }
                            );
                        }).catch(() => {
                            setCargando(false);
                            Alert.alert(
                                'Error',
                                'No fue posible hacer el registro, intente nuevamente'
                            );
                        });

                    }
                }}>
                <View>
                    <Text style={{ textAlign: 'center', color: '#fff' }}>
                        Actualizar libro
                    </Text>
                </View>
            </TouchableOpacity>
        </ScrollView>
    );
}
