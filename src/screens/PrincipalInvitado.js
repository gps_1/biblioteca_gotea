import React from 'react';
import { Alert, Button, TextInput, View, ScrollView, Text, TouchableOpacity } from 'react-native';
import { estilo, baseUrl } from '../libs/MiTema';
import { FontAwesome5 } from '@expo/vector-icons';
import Loader from '../libs/Loader';
//import Logout from './Logout';

export default function PrincipalInvitado(props) {

  return (
    <ScrollView style={estilo.back}>
      <TouchableOpacity onPress={() => props.navigation.navigate('ListadoLibrosInvitado')}
        style={{ backgroundColor: '#ffa833', margin: 60, padding: 25, borderRadius: 5, fontSize: 20, marginVertical: 110 }}>
        <View>
          <Text style={{ fontSize: 20, textAlign: 'center', color: '#000' }}>
            Libros en existencia
          </Text>
        </View>
      </TouchableOpacity>

      <TouchableOpacity onPress={() => props.navigation.navigate('BuscarLinea')}
        style={{ backgroundColor: '#ffa833', margin: 60, padding: 25, borderRadius: 5, fontSize: 20, marginVertical: 20 }}>
        <View>
          <Text style={{ fontSize: 20, textAlign: 'center', color: '#000' }}>
            Búsqueda en línea
          </Text>
        </View>
      </TouchableOpacity>

    </ScrollView>
  );
}