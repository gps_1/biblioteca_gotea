import React, { useState, useEffect } from 'react';
import { Alert, Button, FlatList, Text, TouchableOpacity, View } from 'react-native';
import { baseUrl, estilo } from '../libs/MiTema';
import Loader from '../libs/Loader';
import Axios from 'axios';

export default function ListaUsuarios(props) {

  // Estado para usar el Loader
  const [cargando, setCargando] = useState(false);

  // Estado para guardar el arreglo de objetos usuario
  const [listaUsuarios, setListaUsuarios] = useState([]);

  // Función donde se realiza la petición al servidor para traer los usuarios
  useEffect(() => {
    setCargando(true);
    async function getUsuarios() {
      await Axios({
        method: 'get',
        url: `${baseUrl}/Usuarios/GetUsuarios`
      }).then((response) => {
        setCargando(false);
        setListaUsuarios(response.data);
      }).catch(() => {
        setCargando(false);
        Alert.alert(
          'Error',
          'No fue posible ver el listado de usuarios, intente nuevamente'
        );
      });
    }
    getUsuarios();
  }, []);

  return cargando ? <Loader
    mensaje='Cargando, por favor espere...' /> : (
    <View style={estilo.back}>
      <View style={{ marginHorizontal: 100 }}>
        <Button color='#ffa833' title='Agregar Nuevo Usuario' onPress={() => props.navigation.navigate('AgregarUsuario')}></Button>
      </View>
      <FlatList
        data={listaUsuarios}
        keyExtractor={(item, index) => index.toString()}
        renderItem={(item, index) => {
          return (
            <TouchableOpacity key={`Usuario-${index}`}
              onPress={
                () => props.navigation.navigate('EditarUsuario', {
                  id_usuario: item.item.idUsuario, nom_usuario: item.item.nombre,
                  email_usuario: item.item.email, ap1_usuario: item.item.apellido1,
                  ap2_usuario: item.item.apellido2, pass_usuario: item.item.password,
                  tipo_usuario: item.item.tipo_usuario
                })
              }
              style={{
                marginVertical: 5,
                marginHorizontal: 5,
                marginBottom: 10,                                    
                borderWidth: 1,
                borderRadius: 5,
                borderColor: '#ddd',
                borderBottomWidth: 0,
                shadowColor: '#000000',
                shadowOffset: { width: 0, height: 2 },
                shadowOpacity: 0.8,
                shadowRadius: 5,
                elevation: 4,
              }}
            >
              <Text key={`T-${index}`} style={{ alignSelf: 'center', fontSize: 20, marginTop:17 }}>{item.item.nombre}</Text>
              <Text key={`D-${index}`} style={{ alignSelf: 'center', fontSize: 20 }}>{item.item.apellido1}</Text>
              <Text key={`A-${index}`} style={{ alignSelf: 'center', fontSize: 20 }}>{item.item.apellido2}</Text>
              <View key={`S-${index}`} style={{ marginVertical: 20, paddingHorizontal: 120 }}>
                <Button
                  key={`B-${index}`}
                  color='tomato'
                  title='Eliminar Usuario'
                  onPress={() => {
                    // Validación para no eliminar usuarios administradores
                    // esto para que un usuario admin no se elimine asi mismo
                    // ya que esto puede romper la app
                    if (item.item.tipo_usuario === 3) {
                      Alert.alert(
                        'Alerta',
                        'No es posible eliminar un administrador'
                      );
                    } else if (
                      item.item.tipo_usuario === 1 ||
                      item.item.tipo_usuario === 2
                    ) {
                      Alert.alert(
                        'Eliminar Usuario',
                        '¿Realmente deseas eliminar este usuario?',
                        [
                          {
                            text: 'No',
                          },
                          {
                            text: 'Sí',
                            onPress: () => {
                              setCargando(true);
                              Axios({
                                method: 'delete',
                                url: `${baseUrl}/Usuarios/DeleteUsuario?Id=${item.item.idUsuario}`,
                              })
                                .then(() => {
                                  setCargando(false);
                                  Alert.alert(
                                    'Usuario Eliminado',
                                    'Los datos del usuario fueron eliminados con exito',
                                    [
                                      {
                                        text: 'Continuar',
                                        onPress: () =>
                                          props.navigation.navigate(
                                            'PrincipalAdmin'
                                          ),
                                      },
                                    ],
                                    {
                                      cancelable: false,
                                    }
                                  );
                                })
                                .catch(() => {
                                  setCargando(false);
                                  Alert.alert(
                                    'Error',
                                    'No fue posible hacer la eliminación del usuario, intente nuevamente'
                                  );
                                });
                            },
                          },
                        ]
                      );
                    }
                  }}
                >
                </Button>
              </View>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  )
}