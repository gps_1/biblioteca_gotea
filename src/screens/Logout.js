import React from 'react';
import { Alert, Button, TextInput, View, ScrollView, Text, TouchableOpacity } from 'react-native';
import { estilo, baseUrl } from '../libs/MiTema';
import { FontAwesome5 } from '@expo/vector-icons';

export default function Logout(props) {
  return (
    <View style={{ padding: 120, backgroundColor:"#FFF" }}>
      <View style={{ alignSelf: 'center' }}>
        <FontAwesome5
          name='power-off'
          size={23}
          color='tomato'
        />
      </View>
      <Button
        color='tomato'
        title='CERRAR SESIÓN'
        onPress={() => {
          Alert.alert('Alerta', `¿Estas seguro de salir?`,
            [
              {
                text: 'Cancelar',
              }
              ,
              {
                text: 'Aceptar',
                onPress: () => {
                  props.navigation.replace('Login');
                }
              }
            ],
            {
              cancelable: false
            }
          );
        }}
      />
    </View>
  );
}