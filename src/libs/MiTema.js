import { StyleSheet } from 'react-native';

/* Archivo de estilos que se llama en la screen que se requiera (es el css) */
export const estilo = StyleSheet.create({
    textInput: {
        padding: 12,
        borderRadius: 5,
        margin: 10,
        borderColor: '#335eff',
        borderWidth: 1,
        fontSize: 14,
    },
    container: {
        flex: 1,
        backgroundColor: '#4b0082',
        alignItems: 'center',
        justifyContent: 'center',
    },
    input: {
        borderColor: '#000',
        borderWidth: 1,
        margin: 5,
        marginHorizontal: 15,
        padding: 10,
        borderRadius: 5,
        fontSize: 20,
        textAlign: 'center',
        marginVertical: 15
    },
    boton: {
        borderWidth: 1,
        backgroundColor: '#4b0082',
        padding: 10,
        borderRadius: 5,
        marginHorizontal: 70,
        marginVertical: 15,
        fontSize: 18
    },
    link: {
        textDecorationColor: '#1CAFC3',
        textDecorationStyle: 'solid',
        color: '#000',
        fontStyle: 'italic',
        textAlign: 'center',
        fontSize: 18,
        textDecorationLine: 'underline',

    },
    back:{
        backgroundColor:"#FFF"
    }
});

export const baseUrl = 'http://goteaapi-001-site1.itempurl.com/api';
export const baseUrl2 = 'http://dtai.uteq.edu.mx/~jimger188/SAZURA/';