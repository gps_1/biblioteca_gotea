import React from 'react';
import { View, ActivityIndicator, Text } from 'react-native';

export default function Loader(props) {
    return (
        <View style={{
            flex: 1,
            alignContent: "center",
            alignItems: "center",
            justifyContent: "center"
        }}>
            <ActivityIndicator size='large' color='#4b0082' />
            <Text style={{ marginTop: 15, letterSpacing: 1 }}>
                {props.mensaje}
            </Text>
        </View>
    );
}